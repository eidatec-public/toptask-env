FROM ubuntu:18.04
USER root

RUN apt update
RUN apt install -y nginx npm mysql-client curl
RUN npm install -g n
RUN n 14.8.0
